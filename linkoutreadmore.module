<?php

/**
 * Implements hook_element_info_alter().
 *
 * Add extra processing to Link Out Read More ready widgets.
 */
function linkoutreadmore_element_info_alter(&$type) {
  // Hard coding the element and widget type to link_field here, for now.
  if (isset($type['link_field'])) {
    $type['link_field']['#process'][] = 'linkoutreadmore_element_process';
    // This is terribly annoying that we must replace the 'link_field' theme
    // function, but there it hardcodes what elements are output and the
    // one-line patch of drupal_render($element) to theme_link_field() breaks
    // Drupal.  This is bad, Drupal.  Quite bad.
    $type['link_field']['#theme'] = 'linkoutreadmore_field';
  }
}

/**
 * Implements hook_theme().
 */
function linkoutreadmore_theme() {
  return array(
    'linkoutreadmore_field' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * FAPI theme for an individual text elements.
 */
function theme_linkoutreadmore_field($vars) {
  drupal_add_css(drupal_get_path('module', 'link') .'/link.css');

  $element = $vars['element'];
  // Prefix single value link fields with the name of the field.
  if (empty($element['#field']['multiple'])) {
    if (isset($element['url']) && !isset($element['title'])) {
      unset($element['url']['#title']);
    }
  }

  $output = '';
  $output .= '<div class="link-field-subrow clearfix">';
  if (isset($element['title'])) {
    $output .= '<div class="link-field-title link-field-column">'. drupal_render($element['title']) .'</div>';
  }
  $output .= '<div class="link-field-url'. (isset($element['title']) ? ' link-field-column' : '') .'">'. drupal_render($element['url']) .'</div>';
  $output .= '</div>';
  if (!empty($element['attributes']['target'])) {
    $output .= '<div class="link-attributes">'. drupal_render($element['attributes']['target']) .'</div>';
  }
  if (!empty($element['attributes']['title'])) {
    $output .= '<div class="link-attributes">'. drupal_render($element['attributes']['title']) .'</div>';
  }
  // Everything until here was an exact copy of theme_link_field().
  $output .= drupal_render($element['linkoutreadmore']);

  return $output;
}

/**
 * Implements hook_form_alter().
 */
function linkoutreadmore_form_field_ui_field_edit_form_alter(&$form, $form_state) {
  if (in_array($form['instance']['widget']['type']['#value'], linkoutreadmore_field_types())) {
    $field = $form['#field'];
    $instance = field_info_instance($form['instance']['entity_type']['#value'], $form['instance']['field_name']['#value'], $form['instance']['bundle']['#value']);
    $form['instance']['settings'] += linkoutreadmore_field_widget_settings_form($field, $instance);
  }
}

/**
 * Returns field types that can take Link Out Read More option.
 */
function linkoutreadmore_field_types() {
  // Widget for Link module's link field currently only one we want to modify.
  return array(
    'link_field',
  );
}

/**
 * Configuration form for editing insert settings for a field instance.
 */
function linkoutreadmore_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  $form['linkoutreadmore_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link out read more'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 20,
    '#parents' => array('instance', 'widget', 'settings'),
  );

  $form['linkoutreadmore_fieldset']['linkoutreadmore'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow content editors to indicate that the URL should serve as the URL for the Read More link on teasers'),
    '#default_value' => $settings['linkoutreadmore'],
    '#description' => t('Provides a checkbox after the usual link URL field for indicating that the URL should be used as an out-linking Read More link.'),
  );

  return $form;
}

/**
 * Implements hook_field_widget_info_alter().
 */
function linkoutreadmore_field_widget_info_alter(&$info) {
  $settings = array(
    'linkoutreadmore' => 0,
  );
  foreach (linkoutreadmore_field_types() as $widget_type) {
    if (isset($info[$widget_type]['settings'])) {
      $info[$widget_type]['settings'] += $settings;
    }
  }
}

/**
 * Process function for Link Out Read More-enabled fields.
 *
 * Stolen from insert_element_process().
 */
function linkoutreadmore_element_process($element, $form, $form_state) {
  $element = link_field_process($element, $form, $form_state);
  // Bail out early if the needed properties aren't available. This happens
  // most frequently when editing a field configuration.
  if (!isset($element['#entity_type'])) {
    return $element;
  }

  $item = $element['#value'];
  $field = field_info_field($element['#field_name']);
  $instance = field_info_instance($element['#entity_type'], $element['#field_name'], $element['#bundle']);

  $widget_settings = $instance['widget']['settings'];
  $widget_type = $instance['widget']['type'];

  $attributes = $element['#attributes'];

  // Bail out if Link Out Read More is not enabled on this field.
  if (empty($widget_settings['linkoutreadmore'])) {
    return $element;
  }

  $element['linkoutreadmore'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use this URL for the Read More link'),
    '#description' => t('Check this box if the URL for this link should be used instead of a link to the node on teasers.'),
    '#default_value' => isset($element['#default_value']['attributes']['linkoutreadmore']) ? $element['#default_value']['attributes']['linkoutreadmore'] : 0,
    '#submit' => 'linkoutreadmore_submit',
  );
  // Is there any way to tell if this is a multivalue field?  If so we should do a warning
  return $element;
}

function linkoutreadmore_field_attach_insert($entity_type, $entity) {
  linkoutreadmore_field_attach_save($entity_type, $entity);
}

function linkoutreadmore_field_attach_update($entity_type, $entity) {
  linkoutreadmore_field_attach_save($entity_type, $entity);
}

function linkoutreadmore_field_attach_save($entity_type, $entity) {
  $linkoutreadmore_fields = linkoutreadmore_fields($entity_type, $entity);
  // If bundle does not have Link Out Read More enabled fields, bail early.
  if (!$linkoutreadmore_fields) {
    return;
  }

  $saved = 0;

  // Get entity ID (typically the node ID), but this works regardless).
  list($entity_id) = entity_extract_ids($entity_type, $entity);

  foreach ($linkoutreadmore_fields as $field_name) {
    if ($items = field_get_items($entity_type, $entity, $field_name)) {
      foreach ($items as $delta => $item) {
        if ($item['linkoutreadmore']) {
          // Save only the first one we come across.
          if (!$saved) {
            linkoutreadmore_save($entity_type, $entity_id, $field_name, $delta);
          }
          $saved++;
        }
      }
    }
  }

  // Set messages.
  if ($saved) {
    $msg = t('A link URL will now be used instead of the content path for the read more link.');
    if ($saved > 1) {
      $msg .= '  ' . t('NOTE: More than one URL was checkmarked to be used as a read more link, only the first was used.');
    }
    drupal_set_message($msg);
  }
}

/**
 * Return field names on a bundle that are enabled as Link Out Read More.
 */
function linkoutreadmore_fields($entity_type, $entity) {
  $linkoutreadmore_fields = array();
  // For nodes $entity->type provides the bundle, but we'll keep this universal.
  $bundle = field_extract_bundle($entity_type, $entity);
  $field_instances = field_info_instances($entity_type, $bundle);
  $field_types = linkoutreadmore_field_types();
  foreach ($field_instances as $field_name => $field_instance) {
    if (isset($field_instance['widget']['type']) && in_array($field_instance['widget']['type'], $field_types)) {
      if (isset($field_instance['widget']['settings']['linkoutreadmore']) && $field_instance['widget']['settings']['linkoutreadmore']) {
        $linkoutreadmore_fields[] = $field_name;
      }
    }
  }
  return $linkoutreadmore_fields;
}

/**
 * Save an entity id and field name to the linkoutreadmore table.
 */
function linkoutreadmore_save($entity_type, $entity_id, $field_name, $delta = 0) {
  db_merge('linkoutreadmore')
    ->key(array(
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
    ))
    ->fields(array(
      'field_name' => $field_name,
      'delta' => $delta,
    ))
    ->execute();
}

/**
 * Implements hook_node_view().
 */
function linkoutreadmore_node_view($node, $view_mode, $langcode) {
	$linkouts = linkoutreadmore_by_entity_type('node');
  if (isset($linkouts[$node->nid])) {
	  $linkout = $linkouts[$node->nid];
	  $field_name = $linkout['field_name'];
	  $delta = $linkout['delta'];
	  $items = field_get_items('node', $node, $field_name);
	  $item = $items[$delta];
    $url = $item['url'];
    // If a title attribute is set, use it; otherwise use link title as the
    // title attribute for the Read More link.
    if (isset($item['attributes']['title']) && $item['attributes']['title']) {
	    $title = $item['attributes']['title'];
    }
    elseif (isset($item['title'])) {
	    $title = $item['title'];
    }
    else {
	    $title = t('Read more');
    }
    if ($url) {
      // Build link options array.
      $link_options = array(
        'attributes' => array(
          'title' => $title,
        ),
        'html' => TRUE,
      );

      $node->content['node_link'][0]['#markup'] = l(t('Read more'), $url, $link_options);
    }
  }
}

/**
 * Returns Link Out Read More values for given entity type, indexed by entity ID.
 */
function linkoutreadmore_by_entity_type($entity_type = 'node') {
  $linkouts = &drupal_static(__FUNCTION__, array());
  if (!isset($linkouts[$entity_type])) {
    $linkouts[$entity_type] = db_query('SELECT l.entity_id, l.field_name, l.delta
        FROM {linkoutreadmore} l WHERE l.entity_type = :entity_type',
        array(':entity_type' => $entity_type))
      ->fetchAllAssoc('entity_id', PDO::FETCH_ASSOC);
  }
  return $linkouts[$entity_type];
}
